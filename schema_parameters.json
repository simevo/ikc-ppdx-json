{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "additionalProperties": true,
  "title": "IKC-PPDX JSON schema",
  "description": "JSON file format for IK-CAPE Physical Property Data EXchange",
  "properties": {
    "title": {
      "type": "string",
      "title": "Title",
      "description": "Decriptive title for the file",
      "minLength": 1,
      "maxLength": 50
    },
    "components": {
      "type": "array",
      "format": "table",
      "minItems": 0,
      "uniqueItems": true,
      "title": "Components",
      "description": "List of referenced components",
      "options": {
        "collapsed": true
      },
      "items": {
        "type": "object",
        "additionalProperties": false,
        "title": "Component",
        "description": "Single component",
        "properties": {
          "cid": {
            "type": "string",
            "minLength": 1,
            "title": "Component ID",
            "description": "Unique identifier for the component"
          },
          "name": {
            "type": "string",
            "minLength": 1,
            "title": "Component name",
            "description": "Extended component name"
          },
          "cas": {
            "type": "string",
            "minLength": 1,
            "title": "Component CAS",
            "description": "Component Chemical Abstracts Service Registry Number"
          },
          "formula": {
            "type": "string",
            "minLength": 1,
            "title": "Component formula",
            "description": "Component brute formula"
          }
        }
      }
    },
    "pure_scalar": {
      "type": "array",
      "uniqueItems": true,
      "title": "Pure scalars",
      "description": "Array of pure component constant parameters",
      "options": {
        "collapsed": true
      },
      "items": {
        "type": "object",
        "additionalProperties": false,
        "title": "Pure scalar",
        "description": "Pure component constant parameter",
        "properties": {
          "name": {
            "type": "string",
            "minLength": 1,
            "title": "Parameter name",
            "description": "Pure component constant parameter name",
            "enum": [
              "MW",
              "TC",
              "PC",
              "VC",
              "DENC",
              "ZC",
              "OMEGA",
              "TB",
              "TMP",
              "HLVB",
              "HLSM",
              "HF0V",
              "HF0L",
              "HF0S",
              "GF0V",
              "GF0L",
              "GF0S",
              "S0V",
              "S0L",
              "S0S",
              "PARA",
              "DIPM",
              "RGYR",
              "VOL0",
              
              "UNIQUAC_R",
              "UNIQUAC_Q",
              "RKS_A",
              "RKS_B",
              "PR_A",
              "PR_B",
              "FLORY-HUGGIN_R",
              "DMER_A",
              "DMER_B",
              "TMER_A",
              "TMER_B",
              "HMER_A",
              "HMER_B"
            ]
          },
          "parameters": {
            "type": "array",
            "format": "table",
            "minItems": 1,
            "uniqueItems": true,
            "title": "Parameter values for the components",
            "description": "Pure component constant parameter values for the components",
            "items": {
              "type": "object",
              "additionalProperties": false,
              "title": "Parameter value for a component",
              "description": "Pure component constant parameter value for a component",
              "properties": {
                "cid": {
                  "type": "string",
                  "title": "Component ID",
                  "description": "Unique identifier for the component",
                  "watch": {
                    "components": "components"
                  },
                  "enumSource": [
                    {
                      "source": "components",
                      "value": "{{item.cid}}"
                    }
                  ]
                },
                "value": {
                  "type": "number",
                  "title": "Value of the parameter",
                  "description": "Numerical value of the parameter in SI units"
                }
              }
            }
          }
        }
      }
    },
    "pure_vector": {
      "type": "array",
      "uniqueItems": true,
      "title": "Pure vectors",
      "description": "Array of pure component T and/or P dependent parameters",
      "options": {
        "collapsed": true
      },
      "items": {
        "type": "object",
        "additionalProperties": false,
        "title": "Pure vector",
        "description": "Pure component T and/or P dependent parameter",
        "properties": {
          "name": {
            "type": "string",
            "minLength": 1,
            "title": "Parameter name",
            "description": "Pure component T and/or P dependent parameter name",
            "enum": [
              "PVL",
              "PSV",
              "PSL",
              "HLV",
              "HSV",
              "HSL",
              "CPIG",
              "DIEC",
              "SFTN",
              "VIR2",
              "VIR3",
              "INAD",
              "PTCOW",
              "SOLW",

              "CP",
              "DEN",
              "VOL",
              "VIS",
              "K",
              "H",
              "S",
              "G",
              "HF",
              "GF"
            ]
          },
          "parameters": {
            "type": "array",
            "minItems": 1,
            "uniqueItems": true,
            "title": "Parameter values for the components",
            "description": "Pure component T and/or P dependent parameter values for the components",
            "items": {
              "type": "object",
              "additionalProperties": false,
              "title": "Parameter value for a component",
              "description": "Pure component T and/or P dependent parameter value for a component",
              "properties": {
                "cid": {
                  "type": "string",
                  "minLength": 1,
                  "title": "Component ID",
                  "description": "Unique identifier for the component",
                  "watch": {
                    "components": "components"
                  },
                  "enumSource": [
                    {
                      "source": "components",
                      "value": "{{item.cid}}"
                    }
                  ]
                },
                "equation": {
                  "type": "string",
                  "minLength": 1,
                  "title": "Equation name",
                  "description": "Pure component T and/or P dependent equation name",
                  "enum": [
                    "POLYNOM",
                    "EPOLYNOM",
                    "ANTOINE",
                    "EXANTOINE",
                    "WATSON",
                    "WAGNER",
                    "SUTHERLAND",
                    "KIRCHHOFF",
                    "RACKETT",
                    "CPL1",
                    "CPL2",
                    "VIS1",
                    "ALYLEE",
                    "KIR1",
                    "DIP4",
                    "DIP5",
                    "VIR2",
                    "D104"
                  ]
                },
                "parameters": {
                  "type": "array",
                  "minItems": 1,
                  "format": "table",                  
                  "title": "Array of parameter values for the components",
                  "description": "Array of pure component T and/or P dependent parameter values for the components",
                  "items": {
                    "type": "number"
                  }
                }
              }
            }
          }
        }
      }
    },
    "binary_scalar": {
      "type": "array",
      "uniqueItems": true,
      "title": "Binary scalars",
      "description": "Array of binary constant parameters",
      "options": {
        "collapsed": true
      },
      "items": {
        "type": "object",
        "additionalProperties": false,
        "title": "Binary scalar",
        "description": "Binary constant parameter",
        "properties": {
          "name": {
            "type": "string",
            "minLength": 1,
            "title": "Parameter name",
            "description": "Binary constant parameter name",
            "enum": [
              "PR_K",
              "RKS_K"
            ]
          },
          "parameters": {
            "type": "array",
            "format": "table",
            "minItems": 1,
            "uniqueItems": true,
            "title": "Parameter values for the component pairs",
            "description": "Binary constant parameter values for the component pairs",
            "items": {
              "type": "object",
              "additionalProperties": false,
              "title": "Parameter value for a component pair",
              "description": "Binary constant parameter value for a component pair",
              "properties": {
                "cid1": {
                  "type": "string",
                  "minLength": 1,
                  "title": "First component ID",
                  "description": "Unique identifier for the first component",
                  "watch": {
                    "components": "components"
                  },
                  "enumSource": [
                    {
                      "source": "components",
                      "value": "{{item.cid}}"
                    }
                  ]
                },
                "cid2": {
                  "type": "string",
                  "minLength": 1,
                  "title": "Second component ID",
                  "description": "Unique identifier for the second component",
                  "watch": {
                    "components": "components"
                  },
                  "enumSource": [
                    {
                      "source": "components",
                      "value": "{{item.cid}}"
                    }
                  ]
                },
                "value": {
                  "type": "number",
                  "title": "Value of the parameter",
                  "description": "Numerical value of the parameter in SI units"
                }
              }
            }
          }
        }
      }
    },
    "binary_vector": {
      "type": "array",
      "uniqueItems": true,
      "title": "Binary vectors",
      "description": "Array of binary T and/or P dependent parameters",
      "options": {
        "collapsed": true
      },
      "items": {
        "type": "object",
        "additionalProperties": false,
        "title": "Binary vector",
        "description": "Binary T and/or P dependent parameter",
        "properties": {
          "name": {
            "type": "string",
            "minLength": 1,
            "title": "Parameter name",
            "description": "Binary T and/or P dependent parameter name",
            "enum": [
              "NRTL_TAU",
              "NRTL_S",
              "UNIQUAC_TAU",
              "WILSON_LAMBDA",
              "FLORY-HUGGIN_CHI",
              "HENRY",
              "RKS_KA",
              "RKS_KB",
              "PR_K",
              "DIFV",
              "DIFL"
            ]
          },
          "parameters": {
            "type": "array",
            "format": "table",
            "minItems": 1,
            "uniqueItems": true,
            "title": "Parameter values for the component pairs",
            "description": "Binary T and/or P dependent parameter values for the component pairs",
            "items": {
              "type": "object",
              "additionalProperties": false,
              "title": "Parameter value for a component pair",
              "description": "Binary T and/or P dependent parameter value for a component pair",
              "properties": {
                "cid1": {
                  "type": "string",
                  "minLength": 1,
                  "title": "First component ID",
                  "description": "Unique identifier for the first component",
                  "watch": {
                    "components": "components"
                  },
                  "enumSource": [
                    {
                      "source": "components",
                      "value": "{{item.cid}}"
                    }
                  ]
                },
                "cid2": {
                  "type": "string",
                  "minLength": 1,
                  "title": "Second component ID",
                  "description": "Unique identifier for the second component",
                  "watch": {
                    "components": "components"
                  },
                  "enumSource": [
                    {
                      "source": "components",
                      "value": "{{item.cid}}"
                    }
                  ]
                },
                "parameters": {
                  "type": "array",
                  "format": "table",
                  "minItems": 1,
                  "uniqueItems": true,
                  "title": "Array of parameter values for the component pairs",
                  "description": "Array of binary T and/or P dependent parameter values for component pairs",
                  "items": {
                    "type": "number"
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  "required": [
    "title",
    "components",
    "pure_scalar",
    "pure_vector",
    "binary_scalar",
    "binary_vector"
  ]
}
