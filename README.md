README
======

The IK-CAPE Physical Property Data EXchange JSON (**IKC-PPDX-JSON**) is a proof-of-concept JSON-based file format for the exchange of thermo-physical model parameters.

This demo is a **free** and **open-source** **browser-based** tool that can be used [online](http://libpf.com/sdk/ikc-ppdx-json.html) or run locally to try out the IKC-PPDX-JSON file format.

For the **source code**, check the [gitlab repo](https://gitlab.com/simevo/ikc-ppdx-json).

To see what it's all about, check the blog post: [A JSON-based file format for the exchange of thermo-physical parameters]([http://wp.libpf.com/?p=935).

License
=======

The IKC-PPDX-JSON format and demo (C) Copyright 2015-2016 Paolo Greppi [simevo s.r.l.](http://simevo.com).

**GPLv3 License**:

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

Contains code borrowed from:

- [Bootstrap](http://getbootstrap.com/), the most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web;

- Jeremy Dorn's [JSON Schema Based Editor](https://github.com/jdorn/json-editor) with a [little fix](https://github.com/jdorn/json-editor/issues/432);

- [highlight.js](https://highlightjs.org/), JavaScript syntax highlighting library.

Installation
============

Copy the following files and directories to a known location, for example a ikc-ppdx-json directory:

- index.html

- *.json

- css/

- fonts/

- js/

then start a web server from this location, for example:

    cd ikc-ppdx-json
    python -m SimpleHTTPServer

finally point your browser to the location served by your web server, for example: [http://192.168.0.1:8000/demo.html](http://192.168.0.1:8000/demo.html).
